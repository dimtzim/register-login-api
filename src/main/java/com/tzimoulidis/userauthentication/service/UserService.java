package com.tzimoulidis.userauthentication.service;

import com.tzimoulidis.userauthentication.model.User;

import java.util.List;

public interface UserService {

    User registerNewUser(User user);
    List<User> getAllUsers();
    List<User> deleteUser(String username);
    User getUserByUsername(String username);
}
