package com.tzimoulidis.userauthentication.service;

import com.tzimoulidis.userauthentication.model.Role;

public interface RoleService {

    Role getRoleByName(String name);
}
