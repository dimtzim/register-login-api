package com.tzimoulidis.userauthentication.service.impl;

import com.tzimoulidis.userauthentication.model.Role;
import com.tzimoulidis.userauthentication.model.RoleName;
import com.tzimoulidis.userauthentication.model.User;
import com.tzimoulidis.userauthentication.repository.RoleRepository;
import com.tzimoulidis.userauthentication.repository.UserRepository;
import com.tzimoulidis.userauthentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User registerNewUser(User user) {
        Role userRole = roleRepository.getRoleByName(RoleName.ROLE_USER.name());
        user.setRoles(Collections.singleton(userRole));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> deleteUser(String username) {
        User userToBeDeleted = userRepository.findUserByUsername(username);
        userRepository.delete(userToBeDeleted);
        return userRepository.findAll();
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }
}
