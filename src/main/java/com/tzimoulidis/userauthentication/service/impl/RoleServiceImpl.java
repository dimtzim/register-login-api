package com.tzimoulidis.userauthentication.service.impl;

import com.tzimoulidis.userauthentication.model.Role;
import com.tzimoulidis.userauthentication.repository.RoleRepository;
import com.tzimoulidis.userauthentication.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role getRoleByName(String name) {
        return roleRepository.getRoleByName(name);
    }
}
