package com.tzimoulidis.userauthentication.validator;

import com.tzimoulidis.userauthentication.model.User;
import com.tzimoulidis.userauthentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "The username should not be empty.");
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "The username should be between 6 and 32 characters  ");
        }
        if (userService.getUserByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "The username is already in use");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "The password should not be empty.");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "The password should be between 8 nd 32 characters");
        }
    }
}
