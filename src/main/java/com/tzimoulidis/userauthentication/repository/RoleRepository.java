package com.tzimoulidis.userauthentication.repository;

import com.tzimoulidis.userauthentication.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("select role from Role role where role.name=?1")
    Role getRoleByName(String name);
}
