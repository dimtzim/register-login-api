package com.tzimoulidis.userauthentication.repository;

import com.tzimoulidis.userauthentication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select user from User user where user.username=?1")
    User findUserByUsername(String email);

}
