package com.tzimoulidis.userauthentication.model;

public enum RoleName {

    ROLE_USER,
    ROLE_AdMIN
}
