# Run API
If maven is installed the browse to api's folder and run **mvn spring-boot:run**


# API Documentation

Created with swagger and can be found in **[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)**